global
    log /dev/log len 4096 local0
    log /dev/log len 4096 local1 notice
    chroot /var/lib/haproxy
    stats socket /run/haproxy/admin.sock mode 660 level admin
    stats socket ipv4@127.0.0.1:23646 level admin
    stats timeout 30s
    user haproxy
    group haproxy
    daemon
    maxconn 50000
    spread-checks 2
    server-state-file /etc/haproxy/state/global
    # Default SSL material locations
    ca-base /etc/ssl/certs
    crt-base /etc/ssl/private

    # Default ciphers to use on SSL-enabled listening sockets.
    # For more information, see ciphers(1SSL).
    ssl-default-bind-ciphers ECDHE-RSA-AES256-GCM-SHA384:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-SHA384:ECDHE-RSA-AES128-SHA256:ECDHE-RSA-AES256-SHA:ECDHE-RSA-AES128-SHA:ECDHE-RSA-DES-CBC3-SHA:AES256-GCM-SHA384:AES128-GCM-SHA256:AES256-SHA256:AES128-SHA256:AES256-SHA:AES128-SHA:DES-CBC3-SHA:!aNULL:!eNULL:!EXPORT:!DES:!MD5:!PSK:!RC4:!3DES
    # Keep DH params size at 1024 for Java 6 HTTPS clients
    tune.ssl.default-dh-param 1024

defaults
    log global
    maxconn 50000
    option dontlognull
    # Use all backup servers
    # instead of just the first one
    # if other servers are down
    option allbackups
    timeout connect 5000
    timeout check 30000
    timeout client 90s
    timeout server 90s
    errorfile 400 /etc/haproxy/errors/400.http
    errorfile 403 /etc/haproxy/errors/429.http
    errorfile 408 /etc/haproxy/errors/400.http
    errorfile 429 /etc/haproxy/errors/429.http
    errorfile 500 /etc/haproxy/errors/500.http
    errorfile 502 /etc/haproxy/errors/502.http
    errorfile 503 /etc/haproxy/errors/503.http
    errorfile 504 /etc/haproxy/errors/504.http
    load-server-state-from-file global

listen stats
    bind 0.0.0.0:7331
    mode http
    stats enable
    stats hide-version
    stats realm Haproxy\ Statistics
    stats uri /
    stats auth admin:this-is-a-test-password
    stats admin if TRUE

#---------------------------------------------------------------------
# Sync data between the registry nodes
#---------------------------------------------------------------------
peers registry-peers
    peer fe-registry-01-lb-gstg 10.65.1.101:32768

#---------------------------------------------------------------------
# Frontend configuration
#---------------------------------------------------------------------
frontend http
    bind 0.0.0.0:80
    mode http
    option splice-auto

    acl deny-403-ip src -f /etc/haproxy/front-end-security/deny-403-ips.lst
    http-request deny if deny-403-ip

    timeout client-fin 5s
    log-format %ci:%cp\ [%t]\ %ft\ %sslv\ %b/%s\ %Tq/%Tw/%Tc/%Tr/%Tt\ %ST\ %B\ %U\ %CC\ %CS\ %tsc\ %ac/%fc/%bc/%sc/%rc\ %sq/%bq\ %hr\ %hs\ %{+Q}r
    http-request redirect scheme https code 301

frontend https
    bind 0.0.0.0:443 ssl crt /etc/haproxy/ssl/registry.pem no-sslv3
    mode http
    option splice-auto


    acl deny-403-ip src -f /etc/haproxy/front-end-security/deny-403-ips.lst
    http-request deny if deny-403-ip

    timeout client-fin 5s
    log-format %ci:%cp\ [%t]\ %ft\ %sslv\ %b/%s\ %Tq/%Tw/%Tc/%Tr/%Tt\ %ST\ %B\ %U\ %CC\ %CS\ %tsc\ %ac/%fc/%bc/%sc/%rc\ %sq/%bq\ %hr\ %hs\ %{+Q}r
    capture request header User-Agent len 64

    stick-table type ip size 1m expire 5m store gpc0,conn_rate(1m) peers registry-peers
    tcp-request connection track-sc1 src

    acl has_multiple_slash path_reg /{2,}

    http-request redirect code 308 location https://%[hdr(host)]%[url,regsub(/+,/,g)] if has_multiple_slash
    http-request set-header X-Forwarded-Proto https
    http-request set-header X-SSL %[ssl_fc]

    acl is_canary_path path_beg -f /etc/haproxy/canary-request-paths.lst
    acl is_canary_host hdr_beg(host) -i canary
    acl is_canary req.cook(gitlab_canary) -m str -i true
    acl canary_disabled req.cook(gitlab_canary) -m str -i false
    acl no_be_srvs_canary_registry nbsrv(canary_registry) lt 1

    http-request deny deny_status 400 if is_bad

    ###############################################################
    # Registry routing
    # If there are no healthy canary servers default to the web backend
    use_backend registry if no_be_srvs_canary_registry
    # Use the canary for internal request paths like /gitlab-com/
    use_backend canary_registry if is_canary_path !canary_disabled
    # Use the canary backend if the cookie gitlab_canary=true or if the
    # request is sent to canary.gitlab.com
    use_backend canary_registry if is_canary or is_canary_host
    # Finally, if no other rules match, send the request to the registry backend
    default_backend registry
    ###############################################################

frontend check_http
    bind 0.0.0.0:8001
    mode http
    option splice-auto
    acl no_be_srvs_reg nbsrv(registry) lt 1
    monitor-uri /-/available-http
    monitor fail if no_be_srvs_reg

frontend check_https
    bind 0.0.0.0:8002
    mode http
    option splice-auto
    acl no_be_srvs_reg nbsrv(registry) lt 1
    monitor-uri /-/available-https
    monitor fail if no_be_srvs_reg

#---------------------------------------------------------------------
# Backend configuration
#---------------------------------------------------------------------
backend registry
    mode http
    balance roundrobin
    option forwardfor
    option splice-auto
    timeout server-fin 5s
    option httpchk GET /debug/health HTTP/1.1\r\nHost:\ registry.gitlab.com
    server registry-01-sv-gstg registry-01-sv-gstg.c.gitlab-staging-1.internal:5000 check inter 2s fastinter 1s downinter 5s fall 3 port 5001

backend canary_registry
    mode http
    balance roundrobin
    option forwardfor
    option splice-auto
    timeout server-fin 5s
    option httpchk GET /debug/health HTTP/1.1\r\nHost:\ registry.gitlab.com
